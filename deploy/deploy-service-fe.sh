#!/bin/bash

# WHEN USING PM2 AND EC2 IMAGES

# instance_ip=$1

# ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i /tmp/real-world-app-stevenalves.pem admin@$instance_ip "cd /home/admin/gothinkster-react-redux-realworld-example-app && export PATH=$PATH:/home/admin/.nvm/versions/node/v18.16.0/bin/ && git fetch && git checkout master && git pull origin master && pm2 restart all && exit"

#####################################################################################

# WHEN USING DOCKER CONTAINER

if [ $# -le 1 ]
then
  echo "must have at least 1 arguments"
  echo "#1 - auto scaling group name"
  echo "#2 - ssh key path for ec2 connection (/path/to/key)"
  exit 1
fi

asg_name=$1
key_path=$2

# TO BE REFACTORED
instance_id1=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name $asg_name --query 'AutoScalingGroups[0].Instances[?LifecycleState==`InService`].InstanceId | [0]' --output text --region eu-central-1)
instance_id2=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name $asg_name --query 'AutoScalingGroups[0].Instances[?LifecycleState==`InService`].InstanceId | [1]' --output text --region eu-central-1)

public_address1=$(aws ec2 describe-instances --instance-ids $instance_id1 --query "Reservations[0].Instances[0].PublicIpAddress" --output text --region eu-central-1)
public_address2=$(aws ec2 describe-instances --instance-ids $instance_id2 --query "Reservations[0].Instances[0].PublicIpAddress" --output text --region eu-central-1)

echo "selected instance 1: [id:$instance_id1] [ip_address:$public_address1]"
echo "selected instance 2: [id:$instance_id2] [ip_address:$public_address2]"

# TO BE REFACTORED
# For loop for all instances in the target group or auto scaling to update new container deployment
# images and container updates
image_name=598552768939.dkr.ecr.eu-central-1.amazonaws.com/real-world-fe-app

# UPDATE INSTANCES WITH NEW CONTAINER
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i $key_path admin@$public_address1 'cd /home/admin/realworld-devops && git pull && $(aws ecr get-login --no-include-email --region eu-central-1) && docker rm real-world-realworld-frontend-1 -f && docker rmi $(docker images --filter=reference="598552768939.dkr.ecr.eu-central-1.amazonaws.com/real-world-fe-app" --format "{{.ID}}") && docker compose up -d && exit'

ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i $key_path admin@$public_address2 'cd /home/admin/realworld-devops && git pull && $(aws ecr get-login --no-include-email --region eu-central-1) && docker rm real-world-realworld-frontend-1 -f && docker rmi $(docker images --filter=reference="598552768939.dkr.ecr.eu-central-1.amazonaws.com/real-world-fe-app" --format "{{.ID}}") && docker compose up -d && exit'
