if [ $# -le 5 ]
then
  echo "must have at least 6 arguments"
  echo "#1 - auto scaling group name"
  echo "#2 - your desired git branch for deploy (master|develop|*)"
  echo "#3 - ssh key path for ec2 connection (/path/to/key)"
  echo "#4 - instance warmup config for instance refresh (integer)"
  echo "#5 - min healthy percentage config for instance refresh (integer)"
  echo "#6 - script path location on ec2 instance (/path/to/script)"

  exit 1
fi

asg_name=$1
desired_branch=$2
key_path=$3
instance_warmup=$4
min_healthy_percentage=$5
script_path=$6

echo "get random instance info (instance_id, launch_template, ip_address)"
instance_id=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name $asg_name --query 'AutoScalingGroups[0].Instances[?LifecycleState==`InService`].InstanceId | [0]' --output text)
launch_template=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name $asg_name --query "AutoScalingGroups[0].LaunchTemplate.LaunchTemplateId" --output text)
public_address=$(aws ec2 describe-instances --instance-ids $instance_id --query "Reservations[0].Instances[0].PublicIpAddress" --output text)
echo "selected instance: [id:$instance_id] [launch_template:$launch_template] [ip_address:$public_address]"

echo "detach instance"
aws autoscaling detach-instances --instance-ids $instance_id --auto-scaling-group-name $asg_name --no-should-decrement-desired-capacity > /dev/null

# echo "Updating env"
scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i $key_path $(pwd)/.env admin@$public_address:$script_path

# echo "Updating app"
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i $key_path admin@$public_address "cd $script_path && git reset --hard HEAD && git fetch && git checkout $desired_branch && git pull origin $desired_branch && exit"

echo "create image from updated instance"
timestamp=$(date +%s)
ami=$(aws ec2 create-image --instance-id $instance_id --name "$asg_name v.$timestamp" --query "ImageId" --output text)
until aws ec2 describe-images --image-ids $ami | grep available > /dev/null
do 
  echo "waiting for image build"
  sleep 15
done

echo "modify launch template"
source_version=$(aws ec2 describe-launch-template-versions --launch-template-id $launch_template --query 'LaunchTemplateVersions[?DefaultVersion==`true`].VersionNumber' --output text)
aws ec2 create-launch-template-version --launch-template-id $launch_template --source-version $source_version --launch-template-data "{\"ImageId\":\"$ami\"}" > /dev/null

echo "start auto scaling instance refresh"
aws autoscaling start-instance-refresh --auto-scaling-group-name $asg_name --preferences "{\"InstanceWarmup\": $instance_warmup, \"MinHealthyPercentage\": $min_healthy_percentage}"

echo "killing instance $instance_id"
aws ec2 terminate-instances --instance-ids $instance_id > /dev/null
echo "end deploy s