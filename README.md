# ![React + Redux Example App](project-logo.png)

[![RealWorld Frontend](https://img.shields.io/badge/realworld-frontend-%23783578.svg)](http://realworld.io)

> ### React + Redux codebase containing real world examples (CRUD, auth, advanced patterns, etc) that adheres to the [RealWorld](https://github.com/gothinkster/realworld-example-apps) spec and API.

<a href="https://stackblitz.com/edit/react-redux-realworld" target="_blank"><img width="187" src="https://github.com/gothinkster/realworld/blob/master/media/edit_on_blitz.png?raw=true" /></a>&nbsp;&nbsp;<a href="https://thinkster.io/tutorials/build-a-real-world-react-redux-application" target="_blank"><img width="384" src="https://raw.githubusercontent.com/gothinkster/realworld/master/media/learn-btn-hr.png" /></a>

### [Demo](https://react-redux.realworld.io)&nbsp;&nbsp;&nbsp;&nbsp;[RealWorld](https://github.com/gothinkster/realworld)

Originally created for this [GH issue](https://github.com/reactjs/redux/issues/1353). The codebase is now feature complete; please submit bug fixes via pull requests & feedback via issues.

We also have notes in [**our wiki**](https://github.com/gothinkster/react-redux-realworld-example-app/wiki) about how the various patterns used in this codebase and how they work (thanks [@thejmazz](https://github.com/thejmazz)!)

## AWS Infra

[![Infra](https://file.notion.so/f/s/e6d6e62c-d25a-4098-9a48-a0d82f9551cc/realwarch.png?id=3f1cf90e-610b-4ca9-b125-d1951f540f8d&table=block&spaceId=f04603ba-065c-40c3-9d15-09519e54cfbc&expirationTimestamp=1687372909337&signature=W5yomhLdgC3fsd2NOIfuDd_D6hK7bh9OTBC36B0AJ3w&downloadName=realwarch.png)]

[![Infra](https://file.notion.so/f/s/7d7132ba-9c67-4100-b8fb-46b6e6db41ea/aws1.png?id=27a233e9-f880-45fe-aec3-32210d9accca&table=block&spaceId=f04603ba-065c-40c3-9d15-09519e54cfbc&expirationTimestamp=1687372934211&signature=84PBhZqZ-YZ37u4FsD74f6BIcwoWxAtkd4YF365MkQI&downloadName=aws1.png)]

[![Infra](https://file.notion.so/f/s/eac22614-a7d2-498d-835f-8990f81fef9d/aws2.png?id=b4f8eda0-2f3f-4b7e-bf67-1c891b6f5c20&table=block&spaceId=f04603ba-065c-40c3-9d15-09519e54cfbc&expirationTimestamp=1687372948719&signature=hYymU6zowhrPeXjj4_r9_rAXXV26m8caeHpD-yiRk4E&downloadName=aws2.png)]

[![Infra](https://file.notion.so/f/s/438e9409-f09c-4a56-980a-64bb05b36ce8/aws3.png?id=3c906b33-fc65-4d9a-b3e8-b076b92fe732&table=block&spaceId=f04603ba-065c-40c3-9d15-09519e54cfbc&expirationTimestamp=1687372960070&signature=LN4rRXygq8ciug9H7BPOP1ztQ_-diJXlqZh_DvUuRIw&downloadName=aws3.png)]

## Getting started

You can view a live demo over at https://react-redux.realworld.io/

To get the frontend running locally:

- Clone this repo
- `npm install` to install all req'd dependencies
- `npm start` to start the local server (this project uses create-react-app)

Local web server will use port 4100 instead of standard React's port 3000 to prevent conflicts with some backends like Node or Rails. You can configure port in scripts section of `package.json`: we use [cross-env](https://github.com/kentcdodds/cross-env) to set environment variable PORT for React scripts, this is Windows-compatible way of setting environment variables.
 
Alternatively, you can add `.env` file in the root folder of project to set environment variables (use PORT to change webserver's port). This file will be ignored by git, so it is suitable for API keys and other sensitive stuff. Refer to [dotenv](https://github.com/motdotla/dotenv) and [React](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#adding-development-environment-variables-in-env) documentation for more details. Also, please remove setting variable via script section of `package.json` - `dotenv` never override variables if they are already set.  

### Making requests to the backend API

For convenience, we have a live API server running at https://conduit.productionready.io/api for the application to make requests against. You can view [the API spec here](https://github.com/GoThinkster/productionready/blob/master/api) which contains all routes & responses for the server.

The source code for the backend server (available for Node, Rails and Django) can be found in the [main RealWorld repo](https://github.com/gothinkster/realworld).

If you want to change the API URL to a local server, simply edit `src/agent.js` and change `API_ROOT` to the local server's URL (i.e. `http://localhost:3000/api`)


## Functionality overview

The example application is a social blogging site (i.e. a Medium.com clone) called "Conduit". It uses a custom API for all requests, including authentication. You can view a live demo over at https://redux.productionready.io/

**General functionality:**

- Authenticate users via JWT (login/signup pages + logout button on settings page)
- CRU* users (sign up & settings page - no deleting required)
- CRUD Articles
- CR*D Comments on articles (no updating required)
- GET and display paginated lists of articles
- Favorite articles
- Follow other users

**The general page breakdown looks like this:**

- Home page (URL: /#/ )
    - List of tags
    - List of articles pulled from either Feed, Global, or by Tag
    - Pagination for list of articles
- Sign in/Sign up pages (URL: /#/login, /#/register )
    - Use JWT (store the token in localStorage)
- Settings page (URL: /#/settings )
- Editor page to create/edit articles (URL: /#/editor, /#/editor/article-slug-here )
- Article page (URL: /#/article/article-slug-here )
    - Delete article button (only shown to article's author)
    - Render markdown from server client side
    - Comments section at bottom of page
    - Delete comment button (only shown to comment's author)
- Profile page (URL: /#/@username, /#/@username/favorites )
    - Show basic user info
    - List of articles populated from author's created articles or author's favorited articles

<br />

[![Brought to you by Thinkster](https://raw.githubusercontent.com/gothinkster/realworld/master/media/end.png)](https://thinkster.io)
